# frozen_string_literal: true

# TODO: move this to gitlab-common cookbook
def ubuntu_16_or_higher
  yield if node['platform'] == 'ubuntu' && node['platform_version'].to_f >= 16
end
