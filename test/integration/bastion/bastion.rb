require 'inspec'

control 'Health checks on the ssh service' do
  describe http('http://localhost:80/-/available-ssh') do
    its('status') { should eq 200 }
  end
end
