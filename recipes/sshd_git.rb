# frozen_string_literal: true

# Cookbook:: gitlab-openssh
# Recipe:: sshd_git
# License:: MIT
#
# Copyright:: 2018, GitLab Inc.

template '/etc/ssh/sshd_git_config' do
  source 'sshd_git_config.erb'
  notifies :restart, 'service[sshd_git]', :delayed
  owner 'root'
  group 'root'
  mode '0644'
  variables(
    sshd_config: node['gitlab-openssh']['sshd_git']['sshd_config']
  )
end

systemd_unit 'sshd_git.service' do
  content <<~END_UNIT
    [Unit]
    Description=OpenBSD Secure Shell server
    After=network.target auditd.service
    ConditionPathExists=!/etc/ssh/sshd_not_to_be_run

    [Service]
    ExecStart=/usr/sbin/sshd -D -f /etc/ssh/sshd_git_config
    ExecReload=/bin/kill -HUP $MAINPID
    KillMode=process
    Restart=on-failure
    RestartPreventExitStatus=255
    Type=notify

    [Install]
    WantedBy=multi-user.target
  END_UNIT

  action %i(create enable)
  notifies :restart, 'service[sshd_git]', :delayed
end

service 'sshd_git' do
  action :enable
end
