[![build status](https://gitlab.com/gitlab-cookbooks/gitlab-openssh/badges/master/build.svg)](https://gitlab.com/gitlab-cookbooks/gitlab-openssh/commits/master)
[![coverage report](https://gitlab.com/gitlab-cookbooks/gitlab-openssh/badges/master/coverage.svg)](https://gitlab.com/gitlab-cookbooks/gitlab-openssh/commits/master)

# gitlab-openssh

Wrapper around `openssh` cookbook.

In order to add groups to `AllowGroups` directive of `sshd_confg`, and allow members
of those groups to log in via ssh, specify those as array items:

`node['gitlab-openssh']['allow_groups'] = %w(ninjas robots pirates)`

Please note that there's no merging of the resulting value from other attributes since we
use fully declarative approach now. So, for example, to add users from `build` groups to
be able to administer `role-X` servers, you have to:
 - add `build` to `node['gitlab_users']['groups']` array of `gitlab_users` cookbook
 - add `build` to `node['gitlab-openssh']['allow_groups']` array to be handled by this cookbok.
 - add `build` to `node['authorization']['sudo']['groups']` array to be handled
   by `gitlab_sudo` cookbook
