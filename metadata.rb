# frozen_string_literal: true

name             'gitlab-openssh'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact@gitlab.com'
license          'MIT'
description      'Installs/Configures gitlab-openssh'
version          '1.4.0'
chef_version     '>= 12.1'
issues_url       'https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab-openssh'

depends 'openssh', '1.5.2'
depends 'iptables', '< 8.0.0'
depends 'gitlab_secrets'

supports 'ubuntu', '>= 16.04'
