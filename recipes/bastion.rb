# frozen_string_literal: true

# Cookbook:: gitlab-openssh
# Recipe:: bastion
#
# Copyright:: 2016, GitLab Inc.
#
# All rights reserved - Do Not Redistribute
#

# enable bastion-related healthchecks
if node['gitlab-openssh']['nginx']['distribution'] == 'openresty'
  apt_repository 'openresty' do
    uri 'http://openresty.org/package/ubuntu'
    key 'https://openresty.org/package/pubkey.gpg'
    components ['main']
  end
  apt_package 'openresty'

  link '/etc/nginx' do
    to '/etc/openresty'
    not_if { ::File.directory? '/etc/nginx' }
  end

  file '/etc/openresty/nginx.conf' do
    content <<~NGINX
      events {
        worker_connections 1024;
      }

      http {
        server {
          listen 80 default_server;
          server_name _;
          root /var/www/ssh-is-active;
          index index.html;
          location /-/available-ssh {
            content_by_lua_block {
              if true == os.execute("/bin/systemctl is-active --quiet ssh") then
                ngx.exit(200)
              else
                ngx.exit(503)
              end
            }
          }
        }
      }
    NGINX
    owner 'root'
    group 'root'
    mode '0644'
    notifies :restart, 'service[openresty]', :delayed
  end

  service 'openresty' do
    action %i(enable start)
  end

  # Create an alias from nginx -> openresty for backwards compat. in scripts
  file '/etc/systemd/system/nginx.service' do
    action :delete
    not_if { ::File.symlink? '/etc/systemd/system/nginx.service' }
  end

  link '/etc/systemd/system/nginx.service' do
    to '/usr/lib/systemd/system/openresty.service'
    notifies :run, 'bash[reload systemd]', :immediately
  end

  bash 'reload systemd' do
    action :nothing
    code 'systemctl daemon-reload'
  end
else
  apt_package 'nginx-extras'
  file '/etc/nginx/sites-available/ssh-is-active' do
    content <<~NGINX
      server {
        listen 80 default_server;
        server_name _;
        root /var/www/ssh-is-active;
        index index.html;
        location /-/available-ssh {
          content_by_lua_block {
            if 0 == os.execute("/bin/systemctl is-active --quiet ssh") then
              ngx.exit(200)
            else
              ngx.exit(503)
            end
          }
        }
      }
    NGINX
    owner 'root'
    group 'root'
    mode '0644'
  end

  # remove default since we're defining default_server above
  # Alternative would have been to set it to separate server and update
  # the terraform health check config with a configurable Host: header,
  # but there's no time for this, so its a technical debt and TODO for
  # future you or me.
  file '/etc/nginx/sites-available/default' do
    action :delete
  end

  # This removes the link to the above deleted default file as
  # otherwise nginx will fail on start since the file doesn't exist
  # but the symlink does.
  link '/etc/nginx/sites-enabled/default' do
    action :delete
  end

  service 'nginx' do
    action %i(enable start)
  end

  link '/etc/nginx/sites-enabled/ssh-is-active' do
    to '/etc/nginx/sites-available/ssh-is-active'
    notifies :restart, 'service[nginx]', :delayed
  end
end

# enable same sshd key secret
# We use chef_vault for kitchen validations, which always includes certain keys that this block doesn't handle
data_bag_keys = %w(id chef_type data_bag)
get_secrets(node['openssh']['secrets']['backend'],
            node['openssh']['secrets']['path'],
            node['openssh']['secrets']['key']).reject { |f, _| data_bag_keys.include? f }.each do |f, c|
  file "/etc/ssh/#{f}" do
    content c.gsub(/\\n/, '\n').to_s
    owner 'root'
    group 'root'
    mode '0400'
    sensitive true
    # regen pubkey on change and trigger delayed reload
    notifies :run, "execute[#{f}.pub]", :immediately
    notifies :reload, 'service[ssh]', :delayed
  end

  execute "#{f}.pub" do
    command "ssh-keygen -y -f /etc/ssh/#{f} > /etc/ssh/#{f}.pub"
    action :nothing
  end
end
