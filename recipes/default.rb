# frozen_string_literal: true

# Cookbook:: gitlab-openssh
# Recipe:: default
#
# Copyright:: 2016, GitLab Inc.
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'openssh::default'
