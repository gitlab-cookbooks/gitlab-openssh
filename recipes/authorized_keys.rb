# frozen_string_literal: true

# Cookbook:: gitlab-openssh
# Recipe:: authorized_keys
#
# Copyright:: 2016, GitLab Inc.
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'gitlab-openssh::default'

ubuntu_16_or_higher do
  gitlab_shell_dir = '/opt/gitlab-shell'

  directory gitlab_shell_dir do
    mode '0750'
    owner 'root'
    group 'git'
  end

  cookbook_file "#{gitlab_shell_dir}/authorized_keys" do
    mode '0750'
    owner 'root'
    group 'git'
  end

  node.default['openssh']['server']['authorized_keys_command'] = "#{gitlab_shell_dir}/authorized_keys %u %k"
  node.default['openssh']['server']['authorized_keys_command_user'] = 'git'
end
