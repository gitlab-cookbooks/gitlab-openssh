# frozen_string_literal: true

default['gitlab-openssh']['sshd_git']['sshd_config'] = {
  # NOTE: Be mindful of the variables you add to `AcceptEnv`
  # https://serverfault.com/a/427540
  'AcceptEnv': 'GIT_PROTOCOL',
  'ChallengeResponseAuthentication': 'no',
  'UsePAM': 'no',
  'Port': '2222',
  'AllowGroups': 'git',
  'UseDns': 'no',
  'GssapiAuthentication': 'no',
  'Protocol': '2',
  'PermitRootLogin': 'no',
  'PasswordAuthentication': 'no',
  'ClientAliveInterval': '30',
  'MaxStartups': '100:30:200',
  'PubkeyAcceptedKeyTypes': '+ssh-dss',
  'AuthorizedKeysCommand': '/opt/gitlab-shell/authorized_keys %u %k',
  'AuthorizedKeysCommandUser': 'git',
}
