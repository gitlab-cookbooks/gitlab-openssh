# Only for use when executing from kitchen to update apt packages.

bash 'update apt' do
  code 'apt-get update'
  ignore_failure true
end
