gitlab-openssh CHANGELOG
========================

This file is used to list changes made in each version of the gitlab-openssh cookbook.

0.1.15
-----
- Amarbayar Amarsanaa - Add Okta ASA sshd config line item, if enabled

0.1.14
-----
- Alejandro Rodríguez - Add GIT_PROTOCOL to AcceptEnv to support Git protocol V2

0.1.5
-----
- John Northrup - revert back to OpenSHH cookbook version 1.5.2
0.1.4
-----
- John Northrup - Fix corrected attributes of options to be set
0.1.3
-----
- John Northrup - Fix syntax errors with default recipie
0.1.2
-----
- John Northrup - Removed GSSAPIAuthentication and UseDNS from default SSHD configuration
0.1.1
-----
- John Northrup - Upgraded OpenSSH cookbook dependency to 2.0.0
0.1.0
-----
- Jeroen Nijhof - Initial release of gitlab-openssh

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
