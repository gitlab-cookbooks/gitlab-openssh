# frozen_string_literal: true

# InSpec tests for recipe gitlab-openssh::default

control 'general-openssh-checks' do
  impact 1.0
  title 'General tests for OpenSSH service'
  desc '
    This control ensures that:
      * openssh package is installed
      * openssh service in enabled and running'

  describe package('openssh-server') do
    it { should be_installed }
  end

  describe service('ssh') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end

control 'gitlab-etc-sshd-config-checks' do
  impact 1.0
  title '/etc/ssh/sshd_config should contain correct directives'
  desc '
    This test ensures that /etc/ssh/sshd_config contains only
    correct directives, specified by configuration'

  describe file('/etc/ssh/sshd_config') do
    it { should be_file }
    its('type') { should eq :file }
    its('owner') { should eq 'root' }
    its('group') { should eq 'root' }
    its('mode') { should cmp '0644' }
  end

  describe sshd_config do
    # We set this as 'root' only here, because kitchen connects as root during
    # integration tests. See .kitchen.do.yml and .kitchen.ci.yml
    its('AllowGroups') { should eq 'production testkitchen' }
    its('ChallengeResponseAuthentication') { should eq 'no' }
    its('ClientAliveInterval') { should eq '30' }
    its('MaxStartups') { should eq '100:30:200' }
    its('PasswordAuthentication') { should eq 'no' }
    # We set this as 'yes' only here, because kitchen connects as root during
    # integration tests. See .kitchen.do.yml and .kitchen.ci.yml
    its('PermitRootLogin') { should eq 'yes' }
    its('Protocol') { should eq '2' }
    its('UsePAM') { should eq 'yes' }
  end
end
