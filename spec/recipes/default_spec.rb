# frozen_string_literal: true

require_relative '../spec_helper'

sshd_config = '/etc/ssh/sshd_config'
ubuntu_versions = %w(16.04 18.04 20.04)

describe 'gitlab-openssh::default' do
  ubuntu_versions.each do |v|
    context "with ubuntu #{v} and no attributes" do
      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                   version: v)
                              .converge(described_recipe)
      end
      it 'renders the correct sshd_config' do
        expect(chef_run).to render_file(sshd_config).with_content { |c|
          # TODO: find who's using this and see if we need ssh-dss at all
          expect(c).to match(/^PubkeyAcceptedKeyTypes \+ssh-dss/)
          expect(c).to match(/^ChallengeResponseAuthentication no$/)
          expect(c).to match(/^UsePAM yes$/)
          expect(c).to match(/^AllowGroups production$/)
          expect(c).to match(/^Protocol 2$/)
          expect(c).to match(/^PermitRootLogin no$/)
          expect(c).to match(/^PasswordAuthentication no$/)
          expect(c).to match(/^ClientAliveInterval 30$/)
          expect(c).to match(/^MaxStartups 100:30:200$/)
        }
      end
    end
  end
end

describe 'gitlab-openssh::default' do
  ubuntu_versions.each do |v|
    context "with ubuntu #{v}, when allow_groups attribute is set" do
      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                   version: v) do |node|
          node.normal['gitlab-openssh']['allow_groups'] = %w(build production)
        end.converge(described_recipe)
      end

      it 'adds groups to AllowGroups directive' do
        expect(chef_run).to render_file(sshd_config)
          .with_content(/^AllowGroups build production$/)
      end
    end
  end
end

describe 'gitlab-openssh::default' do
  ubuntu_versions.each do |v|
    context "with ubuntu #{v}, when node['okta_asa']['enable'] attribute is set to true" do
      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                   version: v) do |node|
          node.normal['okta_asa']['enable'] = true
        end.converge(described_recipe)
      end

      it 'adds sftd ssh_ca.pub to TrustedUserCAKeys directive' do
        expect(chef_run).to render_file(sshd_config)
          .with_content('TrustedUserCAKeys /var/lib/sftd/ssh_ca.pub')
      end
    end
  end
end
