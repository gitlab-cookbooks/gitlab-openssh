# frozen_string_literal: true

default['gitlab-openssh']['allow_groups'] = ['production']

# NOTE: this is a string, otherwise openssh cookbooks adds
# several AllowGroups directives
default['openssh']['server']['allow_groups'] = node['gitlab-openssh']['allow_groups'].sort.uniq.join(' ')

# By default turn off DNS lookups and GSS-API Authentication
default['openssh']['server']['use_dns'] = 'no'
default['openssh']['server']['gssapi_authentication'] = 'no'

# Sane values from base roles
default['openssh']['server']['protocol'] = '2'
default['openssh']['server']['permit_root_login'] = 'no'
default['openssh']['server']['password_authentication'] = 'no'
default['openssh']['server']['client_alive_interval'] = '30'
default['openssh']['server']['max_startups'] = '100:30:200'
default['openssh']['server']['Subsystem'] = 'sftp internal-sftp'

# Okta ASA
default['openssh']['server']['trusted_user_c_a_keys'] = '/var/lib/sftd/ssh_ca.pub' if node.read(:okta_asa, :enable) == true

ubuntu_16_or_higher do
  default['openssh']['server']['pubkey_accepted_key_types'] = '+ssh-dss'
end

default['gitlab-openssh']['git-disabled-message'] = 'Git over SSH operations are disabled for the time being'

default['gitlab-openssh']['nginx']['distribution'] =
  if node['platform_version'].to_i > 20
    'openresty'
  else
    'nginx'
  end
