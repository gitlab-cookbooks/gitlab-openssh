# frozen_string_literal: true

gitlab_shell_dir = '/opt/gitlab-shell'

file "#{gitlab_shell_dir}/git-disabled-message.txt" do
  content node['gitlab-openssh']['git-disabled-message']
  mode '0750'
  owner 'root'
  group 'git'
end

cookbook_file "#{gitlab_shell_dir}/git_disabled_authorized_keys" do
  mode '0750'
  owner 'root'
  group 'git'
end

node.default['openssh']['server']['authorized_keys_command'] = "#{gitlab_shell_dir}/git_disabled_authorized_keys %u %k"
