# frozen_string_literal: true

require_relative '../spec_helper'

ubuntu_versions = %w(16.04 18.04 20.04)

describe 'gitlab-openssh::authorized_keys' do
  ubuntu_versions.each do |v|
    context "with ubuntu #{v}" do
      let(:chef_run) do
        ChefSpec::SoloRunner.new(platform: 'ubuntu',
                                 version: v)
                            .converge(described_recipe)
      end

      it 'creates the script directory with the right permissions' do
        expect(chef_run).to create_directory('/opt/gitlab-shell')
          .with_owner('root')
          .with_group('git')
          .with_mode('0750')
      end

      it 'creates the authorized_keys script' do
        expect(chef_run).to create_cookbook_file('/opt/gitlab-shell/authorized_keys')
          .with_owner('root')
          .with_group('git')
          .with_mode('0750')
      end

      it 'renders the authorized_keys_command setup in the sshd_config file' do
        expect(chef_run).to render_file('/etc/ssh/sshd_config')
          .with_content('AuthorizedKeysCommand /opt/gitlab-shell/authorized_keys %u %k')
          .with_content('AuthorizedKeysCommandUser git')
      end
    end
  end
end
